from os import listdir
from os.path import join
import os

path = join(os.sep, "storage", "emulated", "0", "AccDataRec", "rollercoaster-acc")

print(path)
for filename in sorted(listdir(path)):
    if not filename.endswith(".tsv"):
        continue
    print(f"  +- {filename}")
    with open(join(path, filename)) as file:
        abs_t = [int(l.split()[1]) for l in file]
        rel_t = [t - abs_t[0] for t in abs_t]
        delta_t = [rel_t[i + 1] - rel_t[i] for i in range(len(rel_t) - 1)]
        avg_t = 0.0
        for i, t in enumerate(delta_t):
            avg_t += (t - avg_t) / (i + 1)
        print(f"  |    +- Avg: {avg_t:3.0f}")
        print(f"  |    +- Tot: {abs_t[-1] - abs_t[0]}")