from os import listdir
from os.path import join
import os

path = join(os.sep, "storage", "emulated", "0", "AccDataRec", "rollercoaster-acc")

print(path)
files = [f for f in sorted(listdir(path)) if f.endswith(".tsv")]
for i, filename in enumerate(files):
    print(f"  +- [{i:2}] {filename}")

x = int(input("remove "))
y = int(input("out of "))
n = int(input("lines in file "))

with open(join(path, files[n]), "r") as file:
    lines = file.readlines()

with open(join(path, files[n]), "w+") as file:
    for i, line in enumerate(lines):
        if i % y >= x:
            file.write(line)