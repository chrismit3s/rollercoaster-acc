import matplotlib.pyplot as plt
import numpy as np

x = np.arange(-np.pi, np.pi, 0.01)
plt.ion()

fig = plt.figure()
ax = fig.add_subplot()

#ax.spines["top"].set_color("none")
#ax.spines["right"].set_color("none")
#ax.spines["bottom"].set_position("zero")
#ax.spines["left"].set_position("zero")

for phase in np.linspace(0, 10 * np.pi, 1000):
    ax.clear()

    ax.spines["top"].set_color("none")
    ax.spines["right"].set_color("none")
    ax.spines["bottom"].set_position("zero")
    ax.spines["left"].set_position("zero")

    ax.plot(x, np.sin(x + phase), "b-")
    fig.canvas.draw()
    fig.canvas.flush_events()